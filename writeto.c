#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
int main(int argc,char *argv[])
{
	int command,*fd;/*command is used to identify each file,fd will store the file descriptor*/
	if(argc==1)/* if argument count is 1.that is no file is given abort the programm with appropriate error message */
	{
		char *err="ERROR:No file name given in the argument list \n";	/* warrning message to be passed to the standard error */
		write(2,err,strlen(err));	/* write to standard error using it's file descriptor.which is 2 for standard error */
		abort();	/* abort the programm if the file name is not given */
	}
	fd=malloc((argc-1)*sizeof(int));
	int i=0;
	for(command=1;command<argc;command++)
	{
		/* for every value of command i.e every file open it using the open system call and store the respective file descriptor in
		   fd array which we have created dynamically */
		/* for each file set the flags to read,write,execute for all the user,group and others. */
		/* open the file for reading and writing and if the file is not there create it.Also append to the file if the file is already 
		   on the disk and there is already some data */
		fd[command-1]=open(argv[command],O_RDWR|O_CREAT|O_APPEND,S_IRWXU|S_IRWXG|S_IRWXO);
	}
	char ch,wt[2]; /* a variable to read character from the standard input,and a write arry which we will actually write to the file */
	while((ch=getchar())!=EOF) /* read character from the standard input using getchar whcih reads a strem of charcter from the buffer */
	{
		wt[0]=ch;	/*store value of c in 0 index of wt and 1 is a terminating charcater */
		wt[1]='\0';
		for(command=1;command<argc;command++)
		{
			write(fd[command-1],wt,1);
		}
	}
	return 0;


}
