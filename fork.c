#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
int main()
{
	pid_t pid;
	pid=fork();
	if(pid>0)
	{
		while(1)
			printf("this is the parent process\n");
	}
	else if(pid==0)
	{
		execl("/usr/bin/gedit","gedit","/home/a.txt",NULL);
	}
	return 0;
}
