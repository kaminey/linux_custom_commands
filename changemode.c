#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<stdio.h>
#include<string.h>
int main(int argc,char *argv[])
{
	if(argc==1) /*write the error to the standard error */
	{
		fprintf(stderr,"ERROR:no permissions and filename given\n");
		return 1;
	}
	if(argc==2)	/* write the error to the standard error */
	{
		fprintf(stderr,"ERROR:no file name given \n");
		return 1;
	}
	char *permission=argv[1];	/* copy the first argument to the permission pointer to char */
	int i;
	mode_t mode=0;	/* initially set mode to zero */
	for(i=0;i<strlen(permission);i++)	/* read each permission from the permission pointer whcih is pointing to the persmissions */
	{
		if(permission[i]!='-')		/* if permission is set to '_' we don,t have to set any permission for that */
		{
			if(i<=2)		/* for the first three values set the permission for the user */
			{
				if(permission[i]=='r')
				{
					mode=mode|S_IRUSR;
				}
				if(permission[i]=='w')
				{
					mode=mode|S_IWUSR;
				}
				if(permission[i]=='x')
				{
					mode=mode|S_IXUSR;
				}
			}
			else if(i>2 && i<=5)	/*for the next three set the permission for the group */
			{
				if(permission[i]=='r')
				{
					mode=mode|S_IRGRP;
				}
				if(permission[i]=='w')
				{
					mode=mode|S_IWGRP;
				}
				if(permission[i]=='x')
				{
					mode=mode|S_IXGRP;
				}
			}
			else	/*for the last three set the permission for the others */
			{
				if(permission[i]=='r')
				{
					mode=mode|S_IROTH;
				}
				if(permission[i]=='w')
				{
					mode=mode|S_IWOTH;
				}
				if(permission[i]=='x')
				{
					mode=mode|S_IXOTH;
				}
			}
		}
	}
				int sr=chmod(argv[2],mode); /* finally after we have ored evry permission to mode use chmod function call */
				if(sr==-1)/* if the return value is negative then print the required error message to the standard error */
				{
					perror("chmode");
					return 1;
				}
				else	/* output the conformation message to the standard output */
				{
					fprintf(stdout,"Permisiion changed\n");
				}
			return 0;	
}
