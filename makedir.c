#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>
int main(int argc,char *argv[])
{
	if(argc==1)
	{
		fprintf(stderr,"ERROR:Directory name not specified \n");
		return 1;
	}
	int rt=mkdir(argv[1],S_IRWXU);
	if(rt==-1)
	{
		perror("mkdir");
		return 1;
	}
	fprintf(stdout,"Directory created sucessfully\n");
	return 0;
}
