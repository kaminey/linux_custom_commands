#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<stdio.h>
int main(int argc,char *argv[])
{
	if(argc==1)	/*if argument count is empty promt the error message to the standard error */
	{
		char *err="ERROR:argument list is empty \n";
		write(2,err,strlen(err));
		return 1;
	}
	struct stat sb;	/* declare a structure variable sb of type stat which has container for all the necessary information related to file */
	int rt=stat(argv[1],&sb);	/*stat system call write the result into the stricture buffer sb */
	if(rt==-1)			/* error message if the call to stat returns -1 */
	{
		perror("stat");
		return 1;
	}
	fprintf(stdout,"Device id:%ld\n",sb.st_dev);	/*Id of device containg file */
	fprintf(stdout,"File size:%ld\n",sb.st_size);	/*total size in bytes of the file  specified by path */
	fprintf(stdout,"Inode number:%ld\n",sb.st_ino);	/*Inode number of the file */
	fprintf(stdout,"Hard links:%ld\n",sb.st_nlink);	/*Number of hard links to the file */
	fprintf(stdout,"Owner user id:%d\n",sb.st_uid);	/* user id of the file */
	fprintf(stdout,"Gropu user id:%d\n",sb.st_gid);	/* group id of the file */
	fprintf(stdout,"Last access:%ld\n",sb.st_atime);/* time in seconds when the file was last accessed */
	fprintf(stdout,"Last modifies:%ld\n",sb.st_mtime);	/*time when the file was last modified */ 
	fprintf(stdout,"Last status changed:%ld\n",sb.st_ctime);/* last time when the file status was changed */
	printf ("File type: ");		/* determing file type */
	switch (sb.st_mode & S_IFMT)	/*bit mask for the file type bit field */
	 {
		case S_IFBLK:		
		printf("block device node\n");
		break;
		case S_IFCHR:
		printf("character device node\n");
		break;
		case S_IFDIR:
		printf("directory\n");
		break;
		case S_IFIFO:
		printf("FIFO\n");
		break;
		case S_IFLNK:
		printf("symbolic link\n");
		break;
		case S_IFREG:
		printf("regular file\n");
		break;
		case S_IFSOCK:
		printf("socket\n");
		break;
		default:
		printf("unknown\n");
		break;
	}
	return 0;
}
	
