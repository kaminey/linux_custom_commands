#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#define BUFFER_SIZE 1024 /* defines the maximum size of the buffer */
int main(int argc,char *argv[])
{
	int fd1,fd2; /*file descriptors */
	if(argc==1) /*if argc is 1 write an error message to the standard error */
	{	    /* argument can not be empty */
		char *err="ERROR:the argument list is empty \n";
		write(2,err,strlen(err));
		abort();	/*use abort to terminate the programm */
	}
	fd1=open(argv[1],O_RDONLY);	/* open the first file in read only mode */
	if(fd1==-1)
	{
		perror("open");		/* show error message associated with open sysytem call */
		abort();		/* abort the programm */
	}
	fd2=open(argv[2],O_WRONLY|O_TRUNC|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
	if(fd2==-1)
	{
		perror("open");		/* show error message if any */
		abort();		/* abort the programm */
	}
	char buffer[BUFFER_SIZE];	/* buffer with size BUFFER_SIZE whcih is the upper limit upto whcih data can be read in buffer */
	int nr;				/* number of bytes actually read */
	while((nr=read(fd1,buffer,BUFFER_SIZE))>0)	/* while number of bytes is greater than zero whcih means we have data in the file left */
	{
		if(write(fd2,buffer,nr)!=nr)	/* incase of partiall write complaint about this and abort the programm */
		{
			char *err="ERROR:couldn't write whole of the buffer to the specified file \n";	/* error message about the partial write */
			write(2,err,strlen(err));	/* write the error message to the standard error */
			abort();			/* abort the programm */
		}
	}
	char *message="Copied the data sucessfully from one file to another \n"; /*sucess message if the whole of the data is copied */
	write(1,message,strlen(message));	/* write the message to the standard output */
	return 0;
}
