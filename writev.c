#include<stdio.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<string.h>
#include<sys/uio.h>
int main()
{
	struct iovec vec[3];
	int nr;
	int fd,i;
	char *buf[]={"this is my first scattered code\n","here i am implementing writev function call\n","scattered input output used\n"};
	fd=open("a.txt",O_CREAT|O_RDWR|O_TRUNC,S_IRWXU);
	for(i=0;i<3;i++)
	{
		vec[i].iov_base=(char *)buf[i];
		vec[i].iov_len=strlen(buf[i])+1;
	}
	nr=writev(fd,vec,3);
	if(nr==-1)
	{
		perror("writev");
	}
	else
	{
		printf("wrote %d bytes\n",nr);
	}
	close(fd);
	return 0;
}
